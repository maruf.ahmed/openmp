# include <stdio.h>
# include <stdlib.h>
# include <math.h>
# include <time.h>
# include <omp.h>

# define varToStr(var)  #var

//#define DEBUG_step
//#define DEBUG_complexFFT2
//#define DEBUG_complexFFTi
//#define DEBUG_cp1_a
//#define DEBUG_cp1_b
//#define DEBUG_cp2_a
//#define DEBUG_cp2_b
//#define DEBUG_cp2_c
//#define DEBUG_break_step
//#define DEBUG_break_solve

void print_arr (int n, double x[])
{
     for (int i = 0; i < n; i ++)
         printf ("%lf ", x[i]);
     printf ("\n");
}


double uni_dist_rand ( double *seed)
{
     double d2 = 0.2147483647e10;

     double temp ;
     double nextVal;

     temp = (double) *seed;
     temp = fmod (16807.0 * temp, d2);
     *seed = (double) temp;
     nextVal = (double) ( (temp-1.0)/(d2-1.0) );

     return nextVal;
}


void complexArrCopy (int n, double a[], double b[])
{
     for (int i=0; i < n; i ++)
     {
         b [i * 2 + 0] =  a [i*2 + 0];
         b [i * 2 + 1] =  a [i*2 + 1];
     }
}


void step (int n, int mj, double a[], double b[], double c[], double d[],
                     double w[], double direction  )
{
     /* CFFT2 work */

     #ifdef DEBUG_step
     printf ("Entry Function: %s\n", __FUNCTION__);
     printf ("var name: %s=%d\n",varToStr(n),n);
     printf ("var name: %s, \n",varToStr(a));
     print_arr (n*2,a);
     printf ("var name: %s, \n",varToStr(b));
     print_arr (n*2,b);
     printf ("var name: %s, \n",varToStr(c));
     print_arr (n*2,c);
     printf ("var name: %s, \n",varToStr(d));
     print_arr (n*2,d);
     printf ("var name: %s, \n",varToStr(w));
     print_arr (n,w);
     #endif // DEBUG_step

     double ambr, ambu;
     int    ja, jb, jc, jd, jw;
     int    lj, mj2;
     double wjw[2];

     mj2 = 2 * mj;
     lj  = n / mj2;

     #pragma omp parallel \
             shared (a, b, c, d, lj, mj, mj2, direction, w) \
             private (ambr, ambu, ja, jb, jc, jd, jw, wjw)
     #pragma omp for nowait
     for (int j= 0; j < lj; j ++)
     {
         jw = j * mj;
         ja = jw;
         jb = ja;
         jc = j * mj2;
         jd = jc;

         wjw [0] = w [jw *2 + 0];
         wjw [1] = w [jw *2 + 1];

         if ( direction < 0.0)
            wjw [1] = - wjw [1];

         for  (int k = 0; k < mj ; k ++)
         {
             c[ (jc + k)* 2 + 0] = a [ (ja+k)*2 + 0] + b [ (jb + k)*2 + 0];
             c[ (jc + k)* 2 + 1] = a [ (ja+k)*2 + 1] + b [ (jb + k)*2 + 1];

             ambr = a[ (ja + k)*2 + 0 ] - b [ (jb + k)*2 + 0];
             ambu = a[ (ja + k)*2 + 1 ] - b [ (jb + k)*2 + 1];

             d[ (jd + k ) *2 + 0 ] = wjw [0] * ambr - wjw[1] * ambu;
             d[ (jd + k ) *2 + 1 ] = wjw [1] * ambr + wjw[0] * ambu;
             /*printf (" (jd + k ) *2 + 0: %d,  (jd + k ) *2 + 1: %d", (jd + k ) *2 + 0, (jd + k ) *2 + 1);*/
         }

     }

     #ifdef DEBUG_step
     printf ("Exit Function: %s\n", __FUNCTION__);
     printf ("var name: %s, \n",varToStr(a));
     print_arr (n*2,a);
     printf ("var name: %s, \n",varToStr(b));
     print_arr (n*2,b);
     printf ("var name: %s, \n",varToStr(c));
     print_arr (n*2,c);
     printf ("var name: %s, \n",varToStr(d));
     print_arr (n*2,d);
     printf ("var name: %s, \n",varToStr(w));
     print_arr (n,w);
     #endif // DEBUG_step

     #ifdef DEBUG_break_step
     exit(1) ;
     #endif // DEBUG_break_step

}


void complexFFT2 ( int n, double x[], double y[], double w[], double direction)
{
   /******************************
    * int n - size of the array
    *
    * double a [2*n] - input array to be transformed
    *                  will be overwritten during the function call
    *
    * double b [2*n] - output array, either forward or backward FFT of a
    *
    * double c [n] - input array, a table of sines and cosines
    *
    * double direction - forward FFT if +1, backward FFT if -1
    ******************************/

    #ifdef DEBUG_complexFFT2
     printf ("Entry Function: %s\n", __FUNCTION__);
     printf ("var name: %s=%d\n",varToStr(n),n);
     printf ("var name: %s, \n",varToStr(x));
     print_arr (n*2,x);
     printf ("var name: %s, \n",varToStr(y));
     print_arr (n*2,y);
     printf ("var name: %s, \n",varToStr(w));
     print_arr (n,w);
    #endif // DEBUG_complexFFT2

    int m, mj;
    int toggle;

    m  = (int) ( log  (  (double) n ) / log ( 1.99) );
    mj = 1;

    toggle = 1;

    step ( n, mj, &x[0*2 + 0], &x [(n/2)*2 + 0],
                  &y[0*2 + 0], &y [mj*2 + 0]   , w, direction);

    if ( n==2)
        return;

    for (int j=0; j < m - 2; j++)
    {
        mj = mj * 2;

        if (toggle ==1)
        {
            step (n, mj, &y[0*2 + 0], &y[ (n/2)*2 + 0],
                         &x[0*2 + 0], &x[mj*2+0 ], w, direction  );
            toggle = 0;
        }
        else
        {
            step (n, mj, &x[0*2+0], &x[ (n/2)*2 + 0],
                         &y[0*2+0], &y[ mj*2+0 ], w, direction  );
            toggle = 1;
        }
    }
    if (toggle == 1)
        complexArrCopy (n, y, x);

    mj = n/2;

    step (n, mj, &x[0*2+0], &x [ (n/2)*2 + 0],
                 &y[0*2+0], &y [mj*2+0], w, direction);


     #ifdef DEBUG_complexFFT2
     printf ("Entry Function: %s\n", __FUNCTION__);
     printf ("var name: %s=%d\n",varToStr(n),n);
     printf ("var name: %s, \n",varToStr(x));
     print_arr (n*2,x);
     printf ("var name: %s, \n",varToStr(y));
     print_arr (n*2,y);
     printf ("var name: %s, \n",varToStr(w));
     print_arr (n,w);
     #endif // DEBUG_complexFFT2

}


void complexFFTi (int n, double w[])
{
    /************************************
     * int n - size of the array
     *
     * double w[n] - a table of sines and cosines
     *
     *************************************/

     double arg, aw;
     int    n2;
     const double Pi = 3.141592653589793;

     n2 = n/2;
     aw = 2.0 * Pi / ((double) n);

     #ifdef DEBUG_complexFFTi
     printf ("Entry Function: %s\n", __FUNCTION__);
     printf ("var name: %s=%d\n",varToStr(n),n);
     printf ("var name: %s, \n",varToStr(w));
     print_arr (n/2,w);
     #endif // DEBUG_complexFFTi


     #pragma omp parallel  shared (aw, n, w)  private (arg)
     #pragma omp for nowait
      for (int i = 0; i < n2; i ++)
      {
          arg = aw * ( (double) i);
          w[i*2 + 0] = cos (arg);
          w[i*2 + 1] = sin (arg);
      }

     #ifdef DEBUG_complexFFTi
     printf ("Exit Function: %s\n", __FUNCTION__);
     printf ("var name: %s=%d\n",varToStr(n),n);
     printf ("var name: %s, \n",varToStr(w));
     print_arr (n/2,w);
     #endif // DEBUG_complexFFTi
}


void solve (double *wtime)
{
     static double seed;

     int  n;
     int  ln2_max;

     double *w;
     double *x;
     double *y;
     double *z;

     double z0, z1;

     int first_itr;
     double direction;
     double fnm1;
     double error;
     int    nIts;
     //double wtime;
     double flops, mflops;


     ln2_max = 20;
     seed    = 331.0;
     n       = 1;
     nIts    = 1000;
     *wtime   = 0.0;

     for (int ln2 = 1; ln2 <= ln2_max; ln2 ++)
     {
         n = 2 * n;

         /* complex numbers array have twice as much space */
         w = (double *) malloc (     n * sizeof (double));
         x = (double *) malloc ( 2 * n * sizeof (double));
         y = (double *) malloc ( 2 * n * sizeof (double));
         z = (double *) malloc ( 2 * n * sizeof (double));

         for (int i=0; i < 2*n; i++)
         {
             if (i < n) w[i] = 0.0;
             x[i] = y [i] = z[i] = 0.0;
         }

         first_itr = 1;

         for (int icase=0; icase < 2; icase ++)
         {
             if (first_itr == 1)
             {
                 for (int i=0; i < 2 * n; i +=2 )
                 {
                      z0 = uni_dist_rand ( &seed );
                      z1 = uni_dist_rand ( &seed );
                      x[i] = z0;
                      z[i] = z0;
                      x[i + 1] = z [i + 1] = z1;
                 }

                 #ifdef DEBUG_cp1_a
                 printf ("cp1_a\n");
                 printf ("var name: %s=%d\n",varToStr(n),n);
                 printf ("var name: %s\n",varToStr(x));
                 print_arr (n*2,x);
                 printf ("var name: %s\n",varToStr(z));
                 print_arr (n*2,z);
                 #endif // DEBUG_cp1_a

             }
             else
             {
                  #pragma omp parallel shared (n, x, y) private (z0, z1)
                  #pragma omp for nowait
                  for (int i = 0; i < 2 * n; i +=2 )
                  {
                      z0 = 0.0 ;
                      z1 = 0.0 ;
                      x [ i ] = z0;
                      z [ i ] = z0;
                      x [ i+1 ] = z1;
                      z [ i+1 ] = z1;
                  }

                 #ifdef DEBUG_cp1_b
                 printf ("cp1_a\n");
                 printf ("var name: %s=%d\n",varToStr(n),n);
                 printf ("var name: %s\n",varToStr(x));
                 print_arr (n*2,x);
                 printf ("var name: %s\n",varToStr(z));
                 print_arr (n*2,z);
                 #endif // DEBUG_cp1_b
             }

             /* Initialize the sine and cosine table */
             complexFFTi (n, w);

             /* Transform first forward, then backward */
             if (first_itr)
             {

                 #ifdef DEBUG_cp2_a
                 printf ("cp2_a\n");
                 printf ("var name: %s=%d\n",varToStr(n),n);
                 printf ("var name: %s\n",varToStr(x));
                 print_arr (n*2,x);
                 printf ("var name: %s\n",varToStr(y));
                 print_arr (n*2,y);
                 printf ("var name: %s\n",varToStr(w));
                 print_arr (n,w);
                 #endif // DEBUG_cp2_a

                 direction = +1.0;
                 complexFFT2(n, x, y, w, direction);

                 #ifdef DEBUG_cp2_b
                 printf ("cp2_b\n");
                 printf ("var name: %s=%d\n",varToStr(n),n);
                 printf ("var name: %s\n",varToStr(x));
                 print_arr (n*2,x);
                 printf ("var name: %s\n",varToStr(y));
                 print_arr (n*2,y);
                 printf ("var name: %s\n",varToStr(w));
                 print_arr (n,w);
                 #endif // DEBUG_cp2_b

                 direction = -1.0;
                 complexFFT2(n, y, x, w, direction);

                 #ifdef DEBUG_cp2_c
                 printf ("cp2_c\n");
                 printf ("var name: %s=%d\n",varToStr(n),n);
                 printf ("var name: %s\n",varToStr(x));
                 print_arr (n*2,x);
                 printf ("var name: %s\n",varToStr(y));
                 print_arr (n*2,y);
                 printf ("var name: %s\n",varToStr(w));
                 print_arr (n,w);
                 #endif // DEBUG_cp2_c


                 /* Results should be same as the initial data muliplied by N */

                 fnm1 = 1.0 / (double ) n;
                 error = 0.0;

                 for (int i=0; i < 2 * n; i +=2 )
                 {
                     error += pow (z[i]   - fnm1 * x[i]    , 2) +
                              pow (z[i+1] - fnm1 * x[i + 1], 2);

                 }

                 error = sqrt (fnm1 * error);
                 printf (" %c %11d %c %8d %c %15e ", 186, n, 179, nIts, 179, error  );
                 first_itr = 0;


             }
             else
             {
                 *wtime = omp_get_wtime ();
                 for (int it =0; it < nIts; it ++)
                 {
                      direction = +1.0;
                      complexFFT2(n, x, y, w, direction);
                      direction = -1.0;
                      complexFFT2(n, y, x, w, direction);
                 }

                 *wtime = omp_get_wtime() - *wtime ;

                 flops = 2.0 * (double ) nIts * ( 5.0 * (double)n * (double) ln2);

                 mflops = flops / 1.0E+06 / *wtime;

                 printf ("%c %16e %c %16e %c %15f  %c\n", 179, *wtime, 179, *wtime / (double) (2*nIts), 179, mflops, 186 );

             }
         }

         if ( (ln2 % 4) == 0 )
             nIts = nIts / 10;

         if ( nIts < 1)
             nIts = 1;

         free (w);
         free (x);
         free (y);
         free (z);

         #ifdef DEBUG_break_solve
         if (ln2 ==2)
              break;
         #endif // DEBUG_break_solve

     }

     //printf ("\nFunction solve() finished\n");

}

/* REF
 * https://www.asciitable.com/
 */


int main (int argc, char **argv)
{

      double wtime;

      printf ( "Fast Fourier Transform (FFT) in OpenMP\n" );
      printf ( "Total processors: %d\n", omp_get_num_procs ( ) );
      printf ( "Total threads   : %d\n", omp_get_max_threads ( ) );


      printf (" %c", 201 );
      for (int i = 0; i < 99; i ++)
           if (i == 13 || i == 24 || i == 42 || i == 61  || i == 80)
              printf ("%c", 209 );
           else
               printf ("%c", 205 );
      printf ("%c", 187 );

      printf ( "\n %c%12s %c%9s %c%13s    %c%13s     %c%15s   %c%12s %6c\n", 186, "N", 179, "NITS", 179, "Error", 179, "Time", 179,
                                                                                        "Time/Call", 179, "MFLOPS" , 186);
      printf (" %c", 199 );
      for (int i = 0; i < 99; i ++)
            if (i == 13 || i == 24 || i == 42 || i == 61  || i == 80)
                  printf ("%c", 197 );
            else
                  printf ("%c", 196 );
      printf ("%c\n", 182 );

      wtime = 0;
      solve (&wtime);

      printf (" %c", 200 );
      for (int i = 0; i < 99; i ++)
          if (i == 13 || i == 24 || i == 42 || i == 61  || i == 80)
                printf ("%c", 207 );
          else
                printf ("%c", 205 );
      printf ("%c\n", 188 );
      printf ("\n");

      printf( "Total time: %lf secs", wtime);

     return 0;
}
