/************************
* N body simulation
*
* Author: Maruf
**************************/

#include  <omp.h>
#include  <stdlib.h>
#include  <stdio.h>
#include  <math.h>
#include  "initSystem.h"


void simulation ()
{
    double distX, distY, distZ;
    double Fx, Fy, Fz;
    double gmm, r2;
    /*
    double Px1, Py1, Pz1, m1;
    double Px2, Py2, Pz2, m2;

    double Ax, Ay, Az;
    double Vx, Vy, Vz;
    */
    double temp1, temp2, temp3, tempM;

    for (int t = 0; t < steps; t++)
    {

        printf("Cycle %d ", t);
        printf("%.*s", 115, "==============================================================================="
        "===================================================================================================");
        printf("\n");

        /* computeAccelerations */
        /* #pragma omp num_threads(4) */
        #pragma omp parallel for private (distX, distY, distZ, tempM, Fx, Fy, Fz, gmm, r2)
        for(int i=0; i < N; i ++)
        {
              //printf("omp thread num %d\n", omp_get_num_threads());
              Fx = Fy = Fz = 0.0;

              for (int j = 0; j < N; j++)
              {
                    if(i != j )
                    {
                         tempM = Mass[j];
                         distX = PosX[j] - PosX[i];
                         distY = PosY[j] - PosY[i];
                         distZ = PosZ[j] - PosZ[i];

                         r2 = sqrt( distX*distX + distY*distY + distZ*distZ);
                         gmm = GravConst * tempM / pow ( r2, 3);

                         Fx +=  gmm* distX;
                         Fy +=  gmm* distY;
                         Fz +=  gmm* distZ;
                    }
               }
               AccX[i] =  Fx;
               AccY[i] =  Fy;
               AccZ[i] =  Fz;

          }
          #pragma omp barrier


	      /* computePositions     */
	      #pragma omp parallel for
          for (int i=0; i < N; i ++)
          {
               //printf("omp thread num %d\n", omp_get_num_threads());
               PosX[i] = PosX[i] + (  VelX[i]  +  AccX[i]* 0.5 );
               PosY[i] = PosY[i] + (  VelY[i]  +  AccY[i]* 0.5 );
               PosZ[i] = PosZ[i] + (  VelZ[i]  +  AccZ[i]* 0.5 );
          }
          #pragma omp barrier


	      /* computeVelocities    */
	      #pragma omp parallel for
          for (int i=0; i < N; i ++)
          {
               //printf("omp thread num %d\n", omp_get_num_threads());
               VelX[i] = VelX[i] + AccX[i];
               VelY[i] = VelY[i] + AccY[i];
               VelZ[i] = VelZ[i] + AccZ[i];
          }
          #pragma omp barrier


 	      /* resolveCollisions    */
	      #pragma omp parallel for private(temp1, temp2, temp3)
          for (int i=0; i < N-1; i ++)
          {
              //printf("omp thread num %d\n", omp_get_num_threads());
              for(int j = i + 1; j< N; j++)
              {
                   if( PosX[i]==PosX[j] &&  PosY[i]==PosY[j] &&  PosZ[i]==PosZ[j]  )
                   {
                       temp1   = VelX[i];  VelX[i]= VelX[j]; VelX[j]= temp1;
                       temp2   = VelY[i];  VelY[i]= VelY[j]; VelY[j]= temp2;
                       temp3   = VelZ[i];  VelZ[i]= VelZ[j]; VelZ[j]= temp3;
                   }
              }
          }
          #pragma omp barrier

          /* Print values */
          printf("\n");
          printf("%5s: " "%12s%12s%12s\t" "%12s%12s%12s\t", "Body","x","y", "z", "vx", "vy", "vz" );
          printf("%12s%12s%12s\n", "ax", "ay", "az" );
          printValues ();
    }


}
