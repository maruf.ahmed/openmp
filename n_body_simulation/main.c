/************************
* N body simulation
*
* Author: Maruf
**************************/

#include  <omp.h>
#include  <stdlib.h>
#include  <stdio.h>
#include  <math.h>
#include  "initSystem.h"
#include  "simulation.h"

int main (int argc, char *argv[])
{
    char *file = "n_body.dat";

    initSystem (file);
    printValues ();
    simulation ();

    return 0;
}
