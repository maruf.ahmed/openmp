/************************
* N body simulation
*
* Author: Maruf
**************************/

#include  <omp.h>
#include  <stdlib.h>
#include  <stdio.h>
#include  <math.h>
#include  "initSystem.h"


void initSystem (char *file)
{

    FILE * fp = fopen (file, "r");
    if(!fp)
    {
        printf("initSystem: file open error\n");
        fclose(fp) ;
        exit(-1);
    }

    /* <Gravitational Constant> <Number of bodies(N)> <Time step> */
    fscanf(fp, "%lf%d%d", &GravConst, &N, &steps);

    /*Allocate memory */

    PosX = (double *) malloc (N * sizeof (double));
    PosY = (double *) malloc (N * sizeof (double));
    PosZ = (double *) malloc (N * sizeof (double));

    Mass=(double *) malloc (N * sizeof (double));

    VelX = (double *) malloc (N * sizeof (double));
    VelY = (double *) malloc (N * sizeof (double));
    VelZ = (double *) malloc (N * sizeof (double));

    AccX = (double *) malloc (N * sizeof (double));
    AccY = (double *) malloc (N * sizeof (double));
    AccZ = (double *) malloc (N * sizeof (double));

    if(!PosX || !PosY || !PosZ || !Mass || !VelX || !VelY || !VelZ ||
                             !AccX || !AccY || !AccZ )
    {
        printf("initSystem: memory allocation error\n");
        fclose(fp) ;
        exit(-1);
    }

    for (int i=0; i < N; i ++)
    {
        /* <Mass of M1> */
        fscanf(fp, "%lf", &Mass[i]);

        /* <Position of M1 in x,y,z co-ordinates> */
        fscanf(fp, "%lf%lf%lf", &PosX[i],  &PosY[i],  &PosZ[i] );

        /* <Initial velocity of M1 in x,,y,z components> */
        fscanf(fp, "%lf%lf%lf", &VelX[i], &VelY[i], &VelZ[i] );

        AccX[i] = AccY[i] = AccZ[i] = 0.0;
    }

    fclose(fp) ;

}


int printValues ()
{
     if(!PosX || !PosY || !PosZ || !VelX || !VelY || !VelZ || !AccX || !AccY || !AccZ )
     {
         printf("printValues: One of the pointers is not valid\n");
         return -1;
     }

     for(int i=0; i<N; i++)
     {
         printf("%5d: ", i);
         printf("%12.6lf%12.6lf%12.6lf\t", PosX[i],  PosY[i], PosZ[i] );
         printf("%12.6lf%12.6lf%12.6lf\t", VelX [i], VelY[i], VelZ[i] );
         printf("%12.6lf%12.6lf%12.6lf\n", AccX [i], AccY[i], AccZ[i] );
     }
     return 0;
}



