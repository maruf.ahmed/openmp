#ifndef INITSYSTEM_H_INCLUDED
#define INITSYSTEM_H_INCLUDED

double *PosX, *PosY, *PosZ;
double *VelX, *VelY, *VelZ;
double *AccX, *AccY, *AccZ;
double *Mass;
/*
double *arrRX, *arrRY, *arrRZ;
double *finalX, *finalY, *finalZ;
int    *procLoad, *startAddr;
int    nproc;
*/
double GravConst;
int    N, steps;

void initSystem (char *file);
int  printValues ();

#endif // INITSYSTEM_H_INCLUDED
