/************************
* Open C implementation
*
* Main file
* Recursive pi calculation using OpenMP
*
* Author: Maruf
**************************/

#include <stdio.h>
#include <inttypes.h>
#include <omp.h>
#include "recursive_pi.h"

#define N (100*1000*1000ULL)


int main (int argc, char *argv[])
{
    double result;
    double start_time, end_time;

    start_time = omp_get_wtime();
    result = calculate_pi (N);
    end_time = omp_get_wtime();

    printf("Recursive pi calculation in OpenMP\n");
    printf("Value of Pi: %lf \n", result);
    printf("Iterations: %llu \n", N);
    printf("Total threads: %d, time: %f sec \n", omp_get_max_threads(), end_time - start_time);

    return 0;
}
