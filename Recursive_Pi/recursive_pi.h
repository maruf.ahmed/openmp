#ifndef RECURSIVE_PI_H_INCLUDED
#define RECURSIVE_PI_H_INCLUDED

double calculate_pi (unsigned long long n);
double recursive_pi (double h,
                     unsigned depth,
                     unsigned maxdepth,
                     unsigned long long start,
                     unsigned long long n);

#endif // RECURSIVE_PI_H_INCLUDED
