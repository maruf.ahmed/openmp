
/************************
* Open C implementation
*
* Recursive pi calculation using OpenMP
*
* Author: Maruf
**************************/

#include <stdio.h>
#include <omp.h>
#include "recursive_pi.h"

#define MAX_PARALLEL_RECURSIVE_LEVEL 4


double calculate_pi (unsigned long long n)
{
    double result;
    double h;

    result = 0.0;
    h = 1.0 / (double)n;

    #pragma omp parallel shared (result)
    {
         #pragma omp single

         result = recursive_pi (h, 0, MAX_PARALLEL_RECURSIVE_LEVEL, 1, n);
    }

    return (result* h);
}



double recursive_pi (double h, unsigned depth, unsigned maxdepth,
                     unsigned long long start, unsigned long long n)
{
     if (depth < maxdepth)
     {
         double area1, area2, totalarea;

         /* Divide into two halves and calculate recursively */
         #pragma omp task shared (area1)
         area1 = recursive_pi (h, depth + 1, maxdepth, start, n/2 -1 );
         #pragma omp task shared (area2)
         area2 = recursive_pi (h, depth + 1, maxdepth, start + n/2, n/2);

         #pragma omp taskwait
         totalarea = area1 + area2;

         return totalarea;
     }
     else
     {
          /* if maxdepth is reached then do the calculation */

          double area = 0.0;
          double x ;

          for (unsigned long long i=start; i<= start + n; i ++ )
          {
              x = h * (i - 0.5);
              area = area + (4.0 / (1.0 + x * x));
          }
          return area;
     }
}




