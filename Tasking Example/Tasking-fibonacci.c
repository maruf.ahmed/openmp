/************************
* OpenMP C implementation
*
* Tasking example - Fibonacci number
*
* Author: Maruf
**************************/

#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

long fibonacci (long n)
{
    long a, b;

    if (n < 2)
        return n;

    else
    {
        #pragma omp task shared (a) firstprivate (n)
        a = fibonacci(n - 1);

        #pragma omp task shared (b) firstprivate (n)
        b = fibonacci(n - 2);

        #pragma omp taskwait
        return a + b;
    }
}

int main (int argc, char *argv[])
{
    long fn;
    int  tn;

    if (argc != 3)
    {
        printf("Provide the Fibonacci number and number of threads in the command line\n");
    }

    fn = atol (argv[1]);
    tn = atoi (argv[2]);

    omp_set_dynamic(0);
    omp_set_num_threads (tn);

    #pragma omp parallel shared(fn)
    {
        #pragma omp single
        {
            printf("fib (%ld) = %ld\n", fn, fibonacci(fn) );
            printf("Number of threads: %d\n", omp_get_num_threads ());

        }
    }

}
