#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <limits.h>
#include <omp.h>


#define NV 6


void init_graph (int **adj_matrix)
{
     for (int i=0; i < NV; i++)
        for (int j=0; j< NV; j++)
              if (i==j)
                  adj_matrix[i][j] =0;
              else
                  adj_matrix[i][j] = LONG_MAX;

    adj_matrix[0][1] = adj_matrix[1][0] = 40;
    adj_matrix[0][2] = adj_matrix[2][0] = 15;
    adj_matrix[1][2] = adj_matrix[2][1] = 20;
    adj_matrix[1][3] = adj_matrix[3][1] = 10;
    adj_matrix[1][4] = adj_matrix[4][1] = 25;
    adj_matrix[2][3] = adj_matrix[3][2] = 100;
    adj_matrix[1][5] = adj_matrix[5][1] = 6;
    adj_matrix[4][5] = adj_matrix[5][4] = 8;
}


void update_min_dist (int source, int dest, int mv, int *connected, int **adj_matrix,
                                                                 int *min_dist )

{
    for (int i = source; i <= dest; i ++)
        if (connected [i] == 0 )
            if (adj_matrix[mv][i] < LONG_MAX)
                 if ( min_dist[mv] + adj_matrix[mv][i] < min_dist[i])
                           min_dist[i] = min_dist[mv] + adj_matrix[mv][i];

}

void find_nearest (int source, int dest, int *min_dist, int *connected, int *d_nearest, int *v_nearest)
{
    *d_nearest = LONG_MAX;
    *v_nearest = -1;

    for (int i = source; i <= dest; i++)
        if ( connected[i] == 0 && min_dist[i] < *d_nearest)
        {
             *d_nearest = min_dist[i];
             *v_nearest = i;
        }
}


int * dijkstra (int **adj_matrix)
{
     /*
      * INPUT
      * adj_matrix [][]
      *
      * OUUPUT
      * Array with Dijkstra distances
      */

      int   *connected;
      int    mv;
      int    md;
      int   *min_dist;


      /* Allocate and initialize connected nodes*/
      connected = (int *) malloc ( NV * sizeof (int));

      connected[0] = 1;
      for (int i=1; i < NV; i ++)
          connected[i] = 0;

      min_dist = (int *) malloc ( NV * sizeof (int));
      for (int i=0; i < NV; i++)
          min_dist [i] = adj_matrix [0][i];

      #pragma omp parallel   shared (connected, adj_matrix, mv, md, min_dist )
      {
          int  tid;
          int  tot_threads;
          int  start;
          int  end;
          int  local_md;
          int  local_mv;

          tid = omp_get_thread_num ();
          tot_threads = omp_get_num_threads();
          start = ( tid * NV) / tot_threads;
          end   = ((tid + 1)* NV) / (tot_threads - 1);

          #pragma omp single
          {
              printf ("\n");
              printf ("Total threads: %2d \n", tot_threads);
          }
          printf ("Thread: %2d, Start: %2d, End: %2d \n", tid, start, end);

          for (int steps = 1; steps < NV; steps ++)
          {
                #pragma omp single
                {
                     md = LONG_MAX;
                     mv = -1;
                }

               /*
                * Each thread finds the nearest unconnected node in the graph partition
                * Some threads may have no unconnected nodes left
                */
                find_nearest (start, end, min_dist, connected, &local_md, &local_mv);

               /*
                * Update global values in a single thread
                */
                #pragma omp critical
                {
                    if ( local_md < md)
                   {
                        md = local_md;
                        mv = local_mv;
                    }
                 }

                 /*
                  * Barrier makes sure all the values are updated
                  */
                  #pragma omp barrier

                 /*
                  * if mv = -1, then unconnected nodes are not found in any thread
                  * BREAK in the parallel region is not safe, so the iterations is allowed to continue without update.
                  *
                  * Otherwise, connect to the nearest node
                  */

                  #pragma omp single
                  {
                      if ( mv != -1)
                      {
                           connected[mv] = 1;
                           printf ("Thread: %2d, Connecting node: %2d \n", tid, mv);
                      }
                  }
                  /* Make sure all the threads reach this point */
                  #pragma omp barrier

                  /*
                   * Each thread update the MIN_DIST
                   * if path from 0 to MV plus the step form MV to a node is closer than the current record
                   */
                   if ( mv != -1)
                         update_min_dist (start, end, mv, connected, adj_matrix, min_dist);
                   /* Barrier */
                   #pragma omp barrier

             }

             #pragma omp single
             {
                 printf ("\n");
                 printf ("Exit parallel region\n");
             }

       }

       free(connected);
       return min_dist;

}

int main (int argc, char ** argv)
{
      int **adj_matrix;
      int  *min_dist;

      printf (" Dijkstra Algorithm in OpenMP\n");

      adj_matrix = (int **) malloc (NV * sizeof (int *));
      for (int i=0; i < NV ; i ++)
           adj_matrix [i] = (int*) malloc ( NV * sizeof (int));

      init_graph( adj_matrix);

      printf ("Adjacency matrix: \n");
      for (int i= 0; i < NV; i++)
      {
          for (int j=0; j < NV; j++)
               if ( adj_matrix[i][j] >= LONG_MAX  )
                    printf ("   %3s", "X");
               else
                    printf ("   %3d", adj_matrix[i][j]);

          printf ("\n");
      }

      min_dist = dijkstra( adj_matrix);

      printf ("\n");
      printf ("Minimum distances from node 0 :  \n");
      for (int i = 0; i < NV ; i++)
           printf (" %2d  %2d \n", i, min_dist[i]);

      for (int i=0; i < NV ; i ++)
           free(adj_matrix [i]);
      free (adj_matrix ) ;

      free (min_dist);

      printf("End of execution\n");

      return 0;
}
