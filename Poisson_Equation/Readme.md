# Solving poisson equation using OpenMP

This C programm finds approximate solution for Poisson equation in a rectangular region.

The Poisson equation considered here is

```math
 - \left( \frac{ \partial^2 }{\partial x^2} + \frac{ \partial^2 }{\partial y^2} \right)  \varphi (x, y) = f(x,y)  
``` 
Over the rectangle limit $` 0 <= x, y <= 1`$, the exact solution is 
```math
 \varphi (x, y) =  sin ( \pi * x * y)  
``` 
Thus, 
```math
 f (x, y) =  \pi^2 * ( x^2 + y^2 )  * sin ( \pi * x * y)
``` 
With Dirichlet boundary conditions along the lines $`x=0, x=1, y=0,`$ and $`y=1`$

Assuming $`\partial x = \partial y `$, approximate solution by discretizing geometry is 

```math
  U (i, j) =
  \frac{ \left( U (i-1, j) + U (i+1, j) + U (i, j-1)  + U (i, j+1) - 4 * U (i, j) \right) }
  {  dx * dy    } 
```
This is a linear equation system for $` U `$, and can be solved by jacobi iteration.
OpenMP makes the process faster.

### Reference
[Solving the Discrete Poisson Equation using Jacobi, SOR, Conjugate Gradients, and the FFT] (http://people.eecs.berkeley.edu/~demmel/cs267/lecture24/lecture24.html)

[Jacobi Iterative Solution of Poisson’s Equation in 1D] (https://people.sc.fsu.edu/~jburkardt/presentations/jacobi_poisson_1d.pdf)
