#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <omp.h>

# define NX 500
# define NY 500


double u_xxyy (double x, double y)
{
    /* evaluate d/dx d/dx + d/dy d/dy )
     * at exact x, y coordinate point
     */
    double Pi, value;
    Pi = 3.141592653589793;
    value = - Pi * Pi * ( x*x + y*y) * sin (Pi * x * y);
    return value;
}

double u_xy(double x, double y)
{
    /* Evaluate U at x, y coordinate point */
    double Pi, value;
    Pi = 3.141592653589793;
    value = sin (Pi * x * y);
    return value;
}

void jacobi_iteration (int nx, int ny, double dx, double dy, double **f,
                       int it_old, int it_new,
                       double **u, double **new_u)
{
     /* if dx = dy, then
      * - (d/dx d/dx + d/dy d/dy ) U (x,y) can be approximate by
      * ( U(i-1, j) + U(i+1, j) + U(i, j-1) + U(i, j+1) - 4 * U(i,j) ) / dx*dy
      */

      #pragma omp parallel shared (dx, dy, f, it_new, it_old, nx, ny, u, new_u)
      {
          if ( omp_get_thread_num()== 0 )
                  printf ("%5d", omp_get_num_threads ());
          for (int it = it_old +1; it <= it_new; it ++)
          {
              #pragma omp for
              for (int j= 0; j < ny; j++)
                  for (int i = 0; i < nx; i ++)
                      u[i][j] =  new_u[i][j];

              #pragma omp for
              for (int j = 0; j < ny; j ++)
                   for (int i = 0; i < nx; i++)
                        if (i==0 || j ==0 || i == nx-1 || j == ny-1)
                              new_u [i][j]= f[i][j];
                        else
                              new_u [i][j] = (u[i-1][j] + u[i][j+1] +
                                              u[i][j-1] + u[i+1][j] +
                                                     f[i][j] * dx * dy)/4.0;
          }

      }
}


double norm_rms (int nx, int ny, double **a)
{
      /* Returns the normalize RMS value of the matrix
       */

      double v;

      v = 0.0;

      for (int j=0; j < ny; j++)
          for (int i = 0; i < nx; i++)
               v += a[i][j] * a[i][j];

      v = sqrt ( v / (double) (nx*ny));
      return v;
}


double init_RHS_vec (int nx, int ny, double **f)
{
      /* Initiate ride hand side of linear system of
         A * U = F

         In the boundary cases,
                 U(i,j) = F(i,j)
         That is, F stores the boundary values of the solution

         otherwise,
                 (1/DX^2) * ( U(i+1,j) + U(i-1,j) + U(i, j-1) + U(i, j+1) - 4 * U(i,j) = F(i,j)
         That is, interior points of F stores the right hand sides of the Poisson equation

         Where DX is the spacing and

         F(i,j) is the value at X(i) and Y(j) of
                 Pi^2 * (x^2 + y^2) * sin ( Pi * x * y)


         input, NX and NY are the X and Y dimensions.
         Output, double **f is the initialized right hand side data.
       */

        double fnorm;
        double x, y;

        for (int j = 0; j < nx ; j++)
        {
             y = (double) j / (double) (ny-1);
             for (int i = 0; i < nx ; i++)
             {
                  x = (double) i / (double) (nx-1);
                  if ( i == 0 || i == nx-1 || j == 0 || j == ny-1 )
                      f[i][j] =  u_xy( x, y);
                  else
                      f[i][j] =  u_xxyy( x, y);
             }
        }

        fnorm = norm_rms ( nx, ny, f);
        return fnorm;
}


int main (int argc, char **argv)
{
       double dx, dy;
       int    nx, ny;
       double x,y;

       double new_u_norm;
       double u_norm;
       double error;
       double wtime;
       double diff;
       double tolerance;
       double fnorm;

       //int converged;
       int it_new;
       int it_old;

       double **f;
       double **u;
       double **new_u;
       double **diff_u;
       double **exact_u;

       char *converge_msg;

       printf ("Poisson OpenMP\n");

       nx = NX;
       ny = NY;
       dx = 1.0 / (double) (nx - 1);
       dy = 1.0 / (double) (ny - 1);

       printf ("Interior grid dimension [%d X %d] \n", nx, ny);
       printf ("X grid spacing: %f\nY grid spacing: %f \n", dx, dy);

       /* Allocate memory to right hand side of F array */
       f = (double **) malloc (NX * sizeof(double**));
       for (int i = 0; i < NX; i++)
           f[i] = (double *) malloc (NY * sizeof (double*));


       fnorm = init_RHS_vec(nx, ny, f);
       printf ("RMS of F:  %g\n", fnorm);

       /* Allocate NEW U matrix */
       new_u = (double **) malloc (NX * sizeof(double**));
       for (int i = 0; i < NX; i++)
           new_u[i] = (double *) malloc (NY * sizeof (double*));

       /* Store values in NEW U matrix */
       for (int j= 0; j < ny; j++ )
            for (int i = 0; i < nx; i ++)
               if (i==0 || i == nx-1 || j ==0 || j==ny-1)
                    new_u [i][j] = f [i][j];
               else
                    new_u [i][j] = 0.0;

       new_u_norm =  norm_rms (nx, ny, new_u);

       exact_u = (double **) malloc (NX * sizeof(double**));
       for (int i = 0; i < NX; i++)
           exact_u[i] = (double *) malloc (NY * sizeof (double*));

       /* Set exact solutions */
       for (int j= 0; j < ny; j++ )
       {
            y = (double) j / (double) (ny-1);
            for (int i = 0; i < nx; i ++)
           {
                x = (double) i / (double) (nx - 1);
                exact_u [i][j] = u_xy (x, y);
           }
       }
       u_norm = norm_rms (nx, ny, exact_u);

       printf ("RMS of exact solution: %g \n", u_norm);

       diff_u = (double **) malloc (NX * sizeof(double**));
       for (int i = 0; i < NX; i++)
           diff_u[i] = (double *) malloc (NY * sizeof (double*));


       for (int j= 0; j < ny; j++ )
            for (int i = 0; i < nx; i ++)
                diff_u [i][j] = new_u[i][j] - exact_u [i][j];

       error = norm_rms(nx, ny, diff_u);

       printf ( "\n" );
       printf ( " Threads   Iterations     New_U        (New_U - U)      (New_U - Exact_U)\n" );

       printf ("\n");
       printf ( " %15d  %15g                   %14g\n", 0,  new_u_norm, error );

       wtime = omp_get_wtime();

       it_new    = 0;
       tolerance = 0.000001;
       converge_msg = " NOT";

       u = (double **) malloc (NX * sizeof(double**));
       for (int i = 0; i < NX; i++)
           u[i] = (double *) malloc (NY * sizeof (double*));

       while (1)
       {
           it_old = it_new;
           it_new = it_old + 500;

           /* 500 iterations of Jacobi method is performed */
           jacobi_iteration (nx, ny, dx, dy, f, it_old, it_new, u, new_u);

           /* Check for convergence */
           u_norm = new_u_norm;
           new_u_norm = norm_rms (nx, ny, new_u);

           for (int j= 0; j < ny; j++ )
                for (int i = 0; i < nx; i ++)
                     diff_u[i][j] = new_u[i][j] - u[i][j];

           diff = norm_rms (nx, ny, diff_u);

           for (int j= 0; j < ny; j++ )
                for (int i = 0; i < nx; i ++)
                     diff_u[i][j] = new_u[i][j] - exact_u[i][j];

           error = norm_rms (nx, ny, diff_u);

           printf (" %10d  %14g  %16g  %14g\n", it_new, new_u_norm, diff, error);

           if( diff <=  tolerance )
           {
               converge_msg = "";
               break;
           }

       }

       wtime = omp_get_wtime () - wtime;

       printf ( "\n" );
       printf ("Solution DID%s converge\n", converge_msg);
       printf ("Total time: %g Secs\n", wtime);
       return 0;
}
