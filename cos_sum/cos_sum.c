/************************
* Open C implementation
*
* Calculate sum of cosine values
*
* Author: Maruf
**************************/

#include <stdio.h>
#include <math.h>
#include <omp.h>
#include "cos_sum.h"

#define N 1000000


double cos_sum (int n, int * omp_threads)
{
    double sum = 0;

    #pragma omp parallel for reduction ( +: sum)
    for (int i = 0; i < n ; i ++)
    {
        sum += cos(i);
    }
    *omp_threads = omp_get_max_threads();
    return sum;
}


int main (int argc, char ** argv)
{
    double sum = 0;
    double start_time, end_time;
    int omp_threads;

    printf("Calculating sum of %d cosine values\n", N);

    start_time = omp_get_wtime();

    sum = cos_sum (N, &omp_threads);

    end_time = omp_get_wtime();

    printf("Sum is %f\n", sum);
    printf("With %d threads, time is %f sec \n", omp_threads, end_time - start_time);

    return 0;
}
