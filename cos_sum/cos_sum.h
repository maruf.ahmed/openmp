#ifndef COS_SUM_H_INCLUDED
#define COS_SUM_H_INCLUDED

double cos_sum (int n, int * omp_threads);

#endif // COS_SUM_H_INCLUDED
