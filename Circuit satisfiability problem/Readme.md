# Circuit satisfiability problem in OpenMP

It is a decision problem to determine whether a given boolean circuit has a set of inputs that will produce a "true" output.
It is a well known NP-complete problem; therefore, no known polynomial-time algorithm exists. 

This C OpenMP implementation uses exhaustive search in parallel to find all solutions.
Logic gates considered are AND, OR, and NOT.
Input is 23 binary values, and output is one binary value. 
This program finds all the input sets for which the output is "true".
