
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <omp.h>
#include <stdbool.h>

#define N 23

#define DEBUG_OUTPUT

int correct_list[] = {5754104,5754109,5758200,5758205,3656933,
                      3656941,3656957,3661029,3661037,3661053,
                      3665125,7851229,7851261,7855325,7855357};

int false_positive;
int false_negative;
int correct;


void print_binary (int n, bool *bool_arr)
{
      printf ("(");
      for (int j=0; j < n; j++)
            printf (" %d", bool_arr[j]);
      printf (")\n");
}

void int_to_binary (int number, int n, bool *bool_arr )
{
    for (int i = n-1; 0 <= i; i--)
    {
         bool_arr [i] = number % 2;
         number /= 2;
    }
}


int circuit_output (int n, bool *bool_arr)
{
    int value;

    value =
            (  bool_arr[0] ||  bool_arr[1] )

         && ( !bool_arr[1] || !bool_arr[3] )
         && (  bool_arr[2] ||  bool_arr[3] )
         && ( !bool_arr[3] || !bool_arr[4] )

         && (  bool_arr[4] || !bool_arr[5] )
         && (  bool_arr[5] || !bool_arr[6] )
         && (  bool_arr[5] ||  bool_arr[6] )

         && (  bool_arr[6] || !bool_arr[15])
         && (  bool_arr[7] || !bool_arr[8] )
         && ( !bool_arr[7] || !bool_arr[13])

         && (  bool_arr[8] ||  bool_arr[9] )
         && (  bool_arr[8] || !bool_arr[9] )
         && ( !bool_arr[9] || !bool_arr[10] )

         && (  bool_arr[9] ||  bool_arr[11] )
         && (  bool_arr[10]||  bool_arr[11] )
         && (  bool_arr[12]||  bool_arr[13] )

         && (  bool_arr[13]|| !bool_arr[14] )
         && (  bool_arr[14]||  bool_arr[15] )
         && (  bool_arr[14]||  bool_arr[16] )

         && (  bool_arr[17] ||  bool_arr[1] )
         && (  bool_arr[18] || !bool_arr[0] )
         && (  bool_arr[19] ||  bool_arr[1] )

         && (  bool_arr[19] || !bool_arr[18])
         && ( !bool_arr[19] || !bool_arr[9] )
         && (  bool_arr[0]  ||  bool_arr[17] )

         && ( !bool_arr[1]  ||  bool_arr[20] )
         && ( !bool_arr[21] ||  bool_arr[20] )
         && ( !bool_arr[22] ||  bool_arr[20] )

         && ( !bool_arr[21] || !bool_arr[20] )
         && (  bool_arr[22] || !bool_arr[20] );

     return value;
}

int main (int argc, int **argv)
{
     int   ihi,  ilo;
     int   total_solutions;
     int   n;
     double wtime;

     printf ("Circuit Satisfiability problem in OpenMP\n");
     printf ("Number of Threads:  %d \n", omp_get_max_threads ());

     /* Total number of binary vectors to check */
     ilo = 0;
     ihi = 1;
     n   = N;

     for (int i = 1; i <= N; i ++)
          ihi *=2;

     printf ("Number of logic gates N: %d \n", N);
     printf ("Number of input vectors to chk: %d\n", ihi);
     printf ("\n");

     /* Range is divided among the threads */


     total_solutions = 0;

     #ifdef DEBUG_OUTPUT
     false_positive =0;
     false_negative =0;
     correct = 0;
     #endif // DEBUG_OUTPUT

     wtime = omp_get_wtime();



    #pragma omp parallel shared (ihi, ilo, n) \
                            reduction ( + : total_solutions )  reduction ( + : false_positive, false_negative, correct)
    {

         int   tid;
         int   local_ilo, local_ihi;
         int   threads;
         int   local_solutions;
         bool  bool_arr[N];
         int   value;
         int   local_fp;
         int   local_fn;
         int   local_cr;


         threads = omp_get_max_threads();
         tid     = omp_get_thread_num();

         if (tid == 0){
             printf ("  Work division among %d threads.\n", threads);
             printf ("  Thread              Range   \n");
         }
         #pragma omp barrier

         /* Divide the range */
         local_ilo = ( (threads - tid)*ilo    + (tid)   *ihi )/ threads;

         local_ihi = ( (threads - tid -1)*ilo + (tid + 1)*ihi )/ threads;

         printf ("%5d  %4s [ %-10d- %10d ) \n", tid, "", local_ilo, local_ihi);


         local_solutions = 0;
         local_fp = 0;
         local_fn = 0;
         local_cr = 0;


         for (int i= local_ilo; i < local_ihi; i++)
         {
              int_to_binary (i, n, bool_arr);

              value = circuit_output (n, bool_arr);

              #ifdef DEBUG_OUTPUT
              int found = 0;
              for (int k=0; k < sizeof(correct_list)/sizeof(correct_list[0]); k++)
                         if (correct_list[k]==i)
                         {
                              found = 1;
                              break;
                         }

              if (value == 1 && found == 0)
                    local_fp ++;
              else if (value == 0 && found == 1)
                    local_fn ++;
              else if (value == 1 && found == 1)
                    local_cr++;
              //else if (value == 0 && found == 0)
              //     local_cr++;

              #endif // DEBUG_OUTPUT

              if (value == 1)
              {
                   local_solutions ++;
                   #pragma omp critical
                   {
                    printf ("\n Thread: %-2d Local Sol. No: %-3d Iteration: %-8d\n", tid, local_solutions, i );
                    print_binary (n, bool_arr);
                   }
              }
         }

         total_solutions += local_solutions;

         #ifdef DEBUG_OUTPUT
         false_positive += local_fp;
         false_negative += local_fn;
         correct += local_cr;
         #endif // DEBUG_OUTPUT
    }
    #pragma omp barrier

    wtime = omp_get_wtime () - wtime;

    #ifdef DEBUG_OUTPUT
    printf ("Correct: %2d, False Positive: %2d, False Negative: %2d \n", correct, false_positive, false_negative) ;
    #endif // DEBUG_OUTPUT

    printf ("Total solutions: %d \n", total_solutions);
    printf ("Total time: %f \n", wtime );

}
