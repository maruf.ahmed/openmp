/************************
* Open C implementation
*
* Calculate sum of cosine values
*
* Author: Maruf
**************************/
/************************
* REF
* http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.87.5003&rep=rep1&type=pdf
**************************/

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <time.h>
#include <omp.h>

#define N (100L)

void print_arr (long long *arr , long long n);
void swap (long long *a, long long *b);

long long partition (long long *arr, long long low, long long high)
{
     long long i, pivot;

     pivot = arr[high];
     i = low - 1;

     for (long long j = low; j<= high - 1; j++ )
     {
        if (arr[j] < pivot)
        {
            i++;
            swap (&arr[i], &arr[j]);
        }
     }
     swap (&arr[i+1], &arr[high]);

     return (i + 1);
}

void parallel_quicksort (long long  *arr, long long  low, long long  high)
{
     long long i;

     if ( low < high)
     {
         i = partition ( arr, low, high);

         #pragma omp parallel sections
         {
             #pragma omp section
             {
                 parallel_quicksort (arr, low, i - 1 );
             }

             #pragma omp section
             {
                 parallel_quicksort (arr, i + 1, high );
             }
         }
     }
}

long long checkNonDecreasing (long long  *arr, long long  n, long long *count)
{
    long long current_high = arr[0];
    *count = 1;

    for (long long  i = 1; i < n; i ++)
    {
        (*count)++;

        if (arr[i] > current_high)
            current_high = arr[i];
        else if (arr[i] < current_high)
            return -1;
    }
    return 0;
}

void swap (long long *a, long long *b)
{
     long long temp;
     temp = *a;
     *a   = *b;
     *b   = temp;
}

void print_arr (long long  *arr, long long  n)
{
     for (long long i = 0; i < n; i++ )
         printf (" %-lld", arr[i]);
     printf("\n");
}

long long main (int argc, char *argv[])
{
     long long  *arr;
     long long  count;

     arr = (long long  *) malloc ( N * sizeof(long long) );
     for (long long  i=0; i < N; i++)
           arr[i] = rand()% N;

     if (N <= 1000)
          print_arr(arr, N);

     /* call to quicksort */
     printf("Parallel quicksort\n");
     parallel_quicksort (arr, 0, N-1);

     if (N <= 1000)
          print_arr(arr, N);

     printf("\n");
     printf("Non-Decreasing numbers test: ");
     if (checkNonDecreasing  ( arr, N, &count)==0)
         printf("Pass\n");
     else
         printf("Fail");
     printf ("Total elements ckecked: %lld\n", count);

     free (arr);
     return 0;
}
