# Julia Set implementation in OpenMP

Images are produced for different values of constant complex.

<img src="Julia_set-00.png" alt="Julia_set-00" width="500"/>
<img src="Julia_set-01.png" alt="Julia_set-01" width="500"/>

<img src="Julia_set-02.png" alt="Julia_set-02" width="500"/>
<img src="Julia_set-03.png" alt="Julia_set-03" width="500"/>

<img src="Julia_set-04.png" alt="Julia_set-04" width="500"/>
<img src="Julia_set-05.png" alt="Julia_set-05" width="500"/>

 


REF:
[Mandelbrot and Julia Sets](https://www.cs.bham.ac.uk/~pxt/FY-CS/mandelbrot_julia.pdf)
