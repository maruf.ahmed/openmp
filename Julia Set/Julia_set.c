
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <omp.h>

/* Chose only one color sceme */
//#define COLOR_SCEME_1
  #define COLOR_SCEME_2
//#define COLOR_SCEME_3


typedef struct {
    long double  real;
    long double  ima;
} complex_n ;

int TARGA_write (int W, int H, unsigned char *rgb, char *filepath);

complex_n julia_point (int W, int H, long double xl, long double xr, long double yb, long double yt, int i, int j, complex_n const_c)
{
    /*
     * Function returns 1 if a point is in the Julia set
     * z(k+1) = z (k) + c
     * c = -0.8 + 0.156i
     *
     * INPUTS:
     * W * H are image dimensions.
     * xl, xr, yb, yt are left, right, bottom, and top limits, respectively.
     * i, j are input indices to check.
     *
     * OUTPUTS:
     * 1 if the point is in the Julia set
     */

     long double  c_real;
     long double  c_ima;
     long double  a_real;
     long double  a_ima;
     long double  temp;

     complex_n  result;

     c_real = const_c.real;
     c_ima  = const_c.ima;

     long double global_x, global_y;

     /* convert x,y into global coordinates */

     global_x = ( (long double)(W - i - 1) * xl + (long double) i * xr ) / (long double) (W - 1);

     global_y = ( (long double)(H - j - 1) * yb + (long double) j * yt ) / (long double) (H - 1);

     /* X, Y are the real and imaginary components of the complex number
      * A = X + Y * i
      */

      a_real = global_x;
      a_ima  = global_y;

      /* Perform
       * A -> A * A + C
       */

       for (int k = 0; k < 50; k++) //200
       {
           temp  =  a_real * a_real - a_ima  * a_ima  + c_real;
           a_ima =  a_real * a_ima  + a_real * a_ima  + c_ima;

           a_real = temp;

           /* ||A|| > 1000, then return 0 */
           if ( a_real * a_real + a_ima * a_ima  > 1000)
           {
                result.real = 0.01;
                result.ima  = 0.01;
                return result;
           }
      }

      #ifdef COLOR_SCEME_1
      result.real =  result.ima = 1;
      #endif // COLOR_SCEME_1

      #ifdef COLOR_SCEME_2
      result.real = ( a_real * a_real ) /   ( a_real * a_real + a_ima * a_ima );
      result.ima  = ( a_ima * a_ima )   /   ( a_real * a_real + a_ima * a_ima );
      #endif // COLOR_SCEME_2

      #ifdef COLOR_SCEME_3
      result.real = ( a_real * a_real );
      result.ima  = ( a_ima * a_ima );
      #endif // COLOR_SCEME_3

      return result;
}


void julia_set ( unsigned char *rgb, int W, int H, long double xl, long double xr, long double yb, long double yt, complex_n const_c)
{
    /* INPUT
     *  W * H image dimension (height and width in pixels )
     * xL, xR, yB, yT are the left, right, bottom, and top limits.
     *
     * OUTPUT
     * unsigned char * Julia_set [W*H*3],
     * [B, G, R] values, between 0 to 255.
     */

     #pragma omp parallel shared ( W, H, xl, xr, yb, yt )
     {
      complex_n   julia_point_val;
      int         index;

      #pragma omp for
      for (int j = 0; j < H; j ++)
          for (int i=0; i < W; i++)
          {
              julia_point_val = julia_point (W, H, xl, xr, yb, yt, i, j, const_c);

              index = 3 * (j * W + i);

              #ifdef COLOR_SCEME_1
               rgb[index]     = 255 * (1 - julia_point_val.real); //B
               rgb[index + 1] = 255 * (1 - julia_point_val.real); //G
               rgb[index + 2] = 255;                         //R
              #endif // COLOR_SCEME_1

              #ifdef COLOR_SCEME_2
              if (julia_point_val.real <=0.01)
              {
                   rgb[index]     =  01;   //B
                   rgb[index + 1] =  01;   //G
                   rgb[index + 2] =  01;   //R
              }
              else
              {
                  /*
                   *rgb[index]     =  255 * (julia_point_val.real*log(i)  +  julia_point_val.ima*log(i*i)) *0.89;  //B
                   *rgb[index + 1] =  255 * (julia_point_val.real*log(i)  +  julia_point_val.ima*log(i*i)) *0.99;   //G
                   *rgb[index + 2] =  255 * (julia_point_val.real*log(i)  +  julia_point_val.ima*log(i*i)) *0.79 ;  //R //
                   */
                   rgb[index]     =  255 * (julia_point_val.real*log(i+1)          +  julia_point_val.ima*log(i+1)          );  //B
                   rgb[index + 1] =  255 * (julia_point_val.real*log(i+1)          +  julia_point_val.ima*log(i+1)*log(i+1) );  //G
                   rgb[index + 2] =  255 * (julia_point_val.ima *log(i+1)*log(i+1) +  julia_point_val.ima*log(i+1)          );  //R
             }
             #endif // COLOR_SCEME_2

             #ifdef COLOR_SCEME_3
             if (julia_point_val.real <=0.01)
             {
                   rgb[index]     =  01;   //B
                   rgb[index + 1] =  01;   //G
                   rgb[index + 2] =  01;   //R
             }
             else
             {
                   rgb[index]     =  255 * ( log(julia_point_val.real)             +  log(julia_point_val.ima)  );  //B
                   rgb[index + 1] =  255 * ( log(julia_point_val.real)             +  julia_point_val.ima );  //G
                   rgb[index + 2] =  255 * ( julia_point_val.ima                   +  log(julia_point_val.ima) );  //R

             }
             #endif // COLOR_SCEME_3



          }
     }
     #pragma omp barrier
}


int main (int argc, char ** argv)
{
    /*
     * Consider each point (x, y) in a rectangular domain R = [XL, XR]x [YB, YT]
     *
     * Z is a complex number of form X+Y*i, C is a complex constant.
     *
     * Then, apply the following formula
     * Z(0) = Z, and Z(k+1) = Z(k)^2 + C
     * Here, C = -0.8 + 0.156i is used
     *
     * Then, Julia set 'Z' is set of points in R,
     * such that sequence of values Z(k) remains within the domain R.
     *
     * We create a array of W*H points,
     * and Perform 200 iterations on each point of Z.
     * A point is discarded if |Z| > 1000 at any stage.
     */

     int             H, W;
     unsigned char  *rgb;
     long double     wtime;

     long double x_left, x_right, y_bottom, y_top;

     printf ("\n");
     printf ("Julia Set OpenMP\n");

     H = 1000; //H = 1000;
     W = 1000;
     x_left = -1.5;
     x_right= +1.5;
     y_bottom = -1.5;
     y_top    = +1.5;

     char *file_names [] = {"Julia_set-00.tga","Julia_set-01.tga", "Julia_set-02.tga","Julia_set-03.tga",
                            "Julia_set-04.tga","Julia_set-05.tga", "Julia_set-06.tga","Julia_set-07.tga",
                            "Julia_set-08.tga","Julia_set-09.tga", "Julia_set-10.tga","Julia_set-11.tga"};

     complex_n  const_c[ ] =
     {
       {  .real = -0.8,      .ima  = 0.156 } ,     //[0]
       {  .real = -0.74543,  .ima  = 0.11301 } ,   //[1]
       {  .real = -0.75,     .ima  = 0.11 } ,      //[2]
       {  .real = -0.1,      .ima  = 0.651 } ,     //[3]

       {  .real = -0.7,      .ima  = 0.27015 } ,   //[4]

       {  .real = -0.79,     .ima  = 0.15 } ,     //[5]
       {  .real = -.12,      .ima  = -.77 } ,     //[9]
    };

     for (int i = 0; i < sizeof (const_c)/ sizeof(const_c[0]) ; i++)
     {
           wtime = omp_get_wtime();

           rgb = (unsigned char*) malloc ( (W*H) *3 * sizeof (unsigned char));

           julia_set ( rgb, W, H, x_left, x_right, y_bottom, y_top, const_c[i]);

           wtime = omp_get_wtime() - wtime;

           printf ("\n");
           printf ("Threads used: %d, Time: %g \n", omp_get_max_threads(), wtime);

           TARGA_write (W,H, rgb, file_names[i] );

           free(rgb);
     }
     return 0;
}


int TARGA_write (int W, int H, unsigned char *rgb, char *filepath)
{
     FILE *file;
     int header_size;

     file = fopen ( filepath, "wb" );

     unsigned char   idlength       = 0;
     unsigned char   colourmaptype  = 0;
     unsigned char   datatypecode   = 2;
     unsigned short  colourmaporigin= 0;
     unsigned short  colourmaplength= 0;
     unsigned char   colourmapdepth = 0;
     unsigned short  x_origin       = 0;
     unsigned short  y_origin       = 0;

     unsigned short  width        = W;
     unsigned short  height       = H;
     unsigned char   bitsperpixel = 24;
     unsigned char   imagedescriptor=0;

     header_size = sizeof (idlength) +
                   sizeof (colourmaptype) +
                   sizeof (datatypecode) +
                   sizeof (colourmaporigin) +
                   sizeof (colourmaplength) +
                   sizeof (colourmapdepth) +
                   sizeof (x_origin) +
                   sizeof (y_origin) +
                   sizeof (width) +
                   sizeof (height) +
                   sizeof (bitsperpixel) +
                   sizeof (imagedescriptor) ;

     printf ("TARGA Header size: %d \n", header_size );
     if (header_size != 18)
     {
         printf ("Header size is not correct, unable to write");
         return -1;
     }

     fwrite (&idlength     , sizeof (unsigned char), 1, file);
     fwrite (&colourmaptype, sizeof (unsigned char), 1, file);
     fwrite (&datatypecode , sizeof (unsigned char), 1, file);
     fwrite (&colourmaporigin, sizeof (unsigned char), 2, file);
     fwrite (&colourmaplength, sizeof (unsigned char), 2, file);
     fwrite (&colourmapdepth , sizeof (unsigned char), 1, file);
     fwrite (&x_origin       , sizeof (unsigned char), 2, file);
     fwrite (&y_origin       , sizeof (unsigned char), 2, file);

     fwrite (&width          , sizeof (unsigned short), 1, file);
     fwrite (&height         , sizeof (unsigned short), 1, file);
     fwrite (&bitsperpixel   , sizeof (unsigned char), 1, file);
     fwrite (&imagedescriptor, sizeof (unsigned char), 1, file);

     fwrite (rgb, sizeof (unsigned char), W*H*3, file);

     fclose(file);

     printf ("Image written in File %s\n", filepath);
     return 0;
}
