#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <omp.h>
#include "simulation.h"
#include "initSystem.h"

void simulation ()
{
    double temp_diff;
    int    print_i;

    // epsilon    = 0.001;
    diff       = epsilon;
    iterations = 0;
    print_i    = 1;

    time = omp_get_wtime();

    while (epsilon <= diff)
    {
        /* Save current state */
        #pragma omp parallel for
         for (int i = 0; i < M; i ++)
            for (int j = 0; j < N; j++)
                 prev[i][j] = current [i][j];
        #pragma omp barrier

        #pragma omp for
        for (int i = 1; i < M-1; i ++)
            for (int j = 1; j < N-1; j++)
               current [i][j] = ( prev [i-1][j] + prev [i + 1][j] +
                                  prev [i][j-1] + prev [i][j+1] ) / 4.0;
        #pragma omp barrier

        diff      = 0.0;
        temp_diff = 0.0;
        #pragma omp parallel shared (diff, current, prev) firstprivate (temp_diff)
        {

            #pragma omp for
            for (int i = 1; i < M -1; i ++)
                for (int j = 1; j < N-1; j++)
                    if ( temp_diff < fabs ( current [i][j] - prev [i][j]) )
                            temp_diff = fabs ( current [i][j] - prev [i][j]);

            #pragma omp critical
             if ( diff < temp_diff)
                diff = temp_diff;
         }
         #pragma omp barrier

         iterations ++;
         if (iterations == print_i)
         {
             printf ("%10ld  %lf\n", iterations, diff);
             print_i *=2;
         }

    }
     printf ("%10ld  %lf\n", iterations, diff);
     time = omp_get_wtime() - time ;
}
