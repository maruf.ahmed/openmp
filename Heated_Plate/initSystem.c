#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include "initSystem.h"



void initSystem ()
{

      mean = 0.0;

      current = (double **) malloc (M * sizeof (double *));
      prev    = (double **) malloc (M * sizeof (double *));

      #pragma omp parallel for shared (current, prev)
      for (int i = 0; i < M; i ++)
      {
          current [i] = (double *) malloc (N* sizeof (double));
          prev [i]    = (double *) malloc (N* sizeof (double));
      }

      #pragma omp parallel shared (current)
      {
          #pragma omp for
          for (int i = 1; i < M-1; i ++)
          {
              current [i][0]   = 100.0;
              current [i][N-1] = 100.0;
          }

          #pragma omp for
          for (int j=0; j < N; j++)
          {
              current [M-1][j] = 100.0;
              current [0][j]   =   0.0;
          }

          #pragma omp for reduction ( + : mean )
          for (int i = 1; i < M-1; i ++)
               mean += current [i][0] + current[i][N-1];
          #pragma omp barrier

          #pragma omp for reduction ( + : mean )
          for (int j=0; j < N; j++)
               mean += current [M-1][j] + current [0][N-1];
          #pragma omp barrier

      }
      #pragma omp barrier

      mean =  mean / (double)( 2 * (M+N) - 4 );

      #pragma omp parallel shared (mean, current)
      #pragma omp for
       for (int i = 1; i <M-1; i ++)
          for (int j = 1; j < N-1; j++)
             current [i][j] = mean;
       #pragma omp barrier

}

void print_values (double ** arr, char *varName)
{
     printf(" Variable: %s\n", varName);
     for (int i = 0; i < M; i ++)
     {
         for (int j = 0; j < N; j ++)
         {
              printf(" %8.3lf", arr[i][j] );
         }
         printf("\n");
     }
}


