#ifndef INITSYSTEM_H_INCLUDED
#define INITSYSTEM_H_INCLUDED

double **current;
double **prev;
double   mean;

void initSystem ();
void print_values (double ** arr, char *varName);

int    M, N;

//#define M 10
//#define N 10

#define varToStr(var)  #var

#endif // INITSYSTEM_H_INCLUDED
