#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <omp.h>
#include "initSystem.h"
#include "simulation.h"

int main (int argc, char * argv[])
{
     if (argc != 4)
     {
         printf("Usage: %s, DimM DimN epsilon", argv[0]);
         exit (-1);
     }

     M = atoi (argv[1]);
     N = atoi (argv[2]);
     epsilon = atof (argv[3]);

     printf ("\n") ;
     printf ("Heated place simulation in OpenMP\n");
     printf ("Spatial grid dimension: %d x %d \n", M, N );
     printf ("Epsilon: %e\n", epsilon);
     printf ("Number of threads: %d\n", omp_get_max_threads());

     initSystem ();

     printf("\nMean: %lf\n", mean);
     printf ("\n") ;
     //print_values (current, varToStr(current));
     printf (" Iteration  Difference\n");
     printf (" ---------  ----------\n");
     simulation ();

     printf ("\n") ;
     printf ("Wall clock time: %lf secs", time);

}
